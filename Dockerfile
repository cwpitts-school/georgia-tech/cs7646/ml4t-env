FROM ubuntu:18.04

ENV MINICONDA_URL=https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh

COPY conda_env.yml /
RUN apt update -y && \
	apt install -y wget git && \
	wget -q ${MINICONDA_URL} -O miniconda.sh && \
	bash miniconda.sh -b -p /opt/conda && \
	/opt/conda/bin/conda init bash && \
	. /opt/conda/etc/profile.d/conda.sh && \
	conda deactivate && \
	conda env create -f conda_env.yml && \
	conda activate cs7646
