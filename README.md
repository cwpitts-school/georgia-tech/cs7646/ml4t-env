# ml4t-env

Docker container for CS 7646 class projects, following the environment specification outlined [here](http://quantsoftware.gatech.edu/ML4T_Software_Environment). This container provides an environment with Anaconda Python installed, and the "cs7646" environment activated.

This container image is *unofficial*, and offered as a courtesy to my fellow students. Use of this container *does not constitute a replacement for testing on the buffet machines provided by the University*. By using this container, *you accept sole responsibility for ensuring that your code works in the environment in which it will be tested*, which is **not this image**.
